﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Lab8.Models
{
    public class Ticket
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string subject { get; set; }
        public string desc { get; set; }
        public DateTime createdAt { get; set; }
        [Required]
        public string name { get; set; }
        [Required]
        public string email { get; set; }
        public bool completed { get; set; }
        public bool active { get; set; }
    }
}
